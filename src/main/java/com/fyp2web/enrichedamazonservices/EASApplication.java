package com.fyp2web.enrichedamazonservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EASApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(EASApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(EASApplication.class, args);
        LOGGER.info("Spring App started successfully");
    }

}