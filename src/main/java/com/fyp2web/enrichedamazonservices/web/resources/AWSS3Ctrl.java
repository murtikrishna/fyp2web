package com.fyp2web.enrichedamazonservices.web.resources;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.fyp2web.enrichedamazonservices.aws.services.AWSS3Service;
import com.fyp2web.enrichedamazonservices.aws.services.EmptyBucketException;

@Controller
@RequestMapping(value = "/s3")
public class AWSS3Ctrl {

    @Autowired
    private AWSS3Service service;

    @PostMapping(value = "file/upload")
    public ResponseEntity<String> uploadFile(@RequestPart(value = "files") final MultipartFile[] multipartFiles) {
	String response = "";
	try {
	    for (MultipartFile multipartFile : multipartFiles) {
		service.uploadFile(multipartFile);
		response += "[" + multipartFile.getOriginalFilename() + "] uploaded successfully.\n";
	    }
	    return new ResponseEntity<>(response, HttpStatus.OK);
	} catch (Exception e) {
	    return new ResponseEntity<>(
		    "error came after:-\n" + response
			    + String.format("Error= {} while uploading file.", e.getMessage()),
		    HttpStatus.INTERNAL_SERVER_ERROR);
	}
    }

    @GetMapping(value = "/file/download")
    public ResponseEntity<ByteArrayResource> downloadFile(@RequestParam(value = "fileName") final String keyName) {
	try {
	    final byte[] data = service.downloadFile(keyName);
	    final ByteArrayResource resource = new ByteArrayResource(data);
	    return ResponseEntity.ok().contentLength(data.length).header("Content-type", "application/octet-stream")
		    .header("Content-disposition", "attachment; filename=\"" + keyName + "\"").body(resource);
	} catch (Exception e) {
	    final ByteArrayResource resource = new ByteArrayResource(new byte[10],
		    String.format("Error= {} while uploading file.", e.getMessage()));
	    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resource);
	}

    }

    @GetMapping(value = "/file/search")
    public ResponseEntity<List<String>> searchFile(
	    @RequestParam(value = "fileName", required = false) final String keyName) {
	List<String> list = null;
	try {
	    list = service.searchFileOverContent(keyName);
	    if (list.size() == 0) {
		list.add("File not found");
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(list);
	    }
	    return ResponseEntity.ok(list);
	} catch (Exception e) {
	    if (e.getClass().equals(EmptyBucketException.class)) {
		list = new ArrayList<String>();
		list.add(e.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(list);
	    }
	    list.add("Exception occured while searching the file!\n" + e.getMessage());
	    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(list);
	}

    }

    // deletes single file per request....
    @GetMapping(value = "/file/delete")
    public ResponseEntity<String> deleteFile(@RequestParam(value = "fileName") final String fileName) {
	try {
	    return service.deleteFile(fileName)
		    ? ResponseEntity.status(HttpStatus.OK).body(fileName + ": FILE DELETED SUCCESSFULLY!")
		    : ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
			    .body("File not deleted..please check the portal if any issue elated to authority");
	} catch (Exception e) {
	    e.printStackTrace();
	    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.toString());
	}
    }

}