package com.fyp2web.enrichedamazonservices.web.resources;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fyp2web.enrichedamazonservices.aws.services.AWSS3Service;
import com.fyp2web.enrichedamazonservices.user.services.AppUserForm;
import com.fyp2web.enrichedamazonservices.user.services.AppUserValidator;
import com.fyp2web.enrichedamazonservices.user.services.ApplicationUser;
import com.fyp2web.enrichedamazonservices.user.services.ApplicationUserBucket;
import com.fyp2web.enrichedamazonservices.user.services.UserAccessDAO;

@Controller
public class RegistrationCtrl {

    @Autowired
    UserAccessDAO userDAO;
    @Autowired
    private AWSS3Service service;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    private AppUserValidator validator;

    // setting Vlidator for the controller
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
	binder.setValidator(validator);
    }

    // Show Register page.
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String viewRegister(Model model) {

	AppUserForm form = new AppUserForm();

	model.addAttribute("appUserForm", form);

	return "register";
    }

    // This method is called to save the registration information.
    // @Validated: To ensure that this Form
    // has been Validated before this method is invoked.
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String saveRegister(Model model, @ModelAttribute("appUserForm") @Valid AppUserForm appUserForm,
	    BindingResult result, //
	    final RedirectAttributes redirectAttributes) {

	// Validate result
	if (result.hasErrors()) {
	    return "register";
	}
	// Registration Routines starts here...

	// adding user to User table and sync new user registration...
	boolean userSaved = false;
	boolean bucketCreated = false;
	try {
	    // building application user...
	    ApplicationUser user = new ApplicationUser();
	    long userId = (new Date()).getTime();
	    user.setId(userId);
	    user.setUsername(appUserForm.getUserName());
	    user.setEmail(appUserForm.getEmail());
	    user.setPassword(encoder.encode(appUserForm.getPassword()));
	    user.setFullName(appUserForm.getFirstName(), appUserForm.getLastName());
	    user.setGender(appUserForm.getGender());
	    // adding bucket name assigned to new user...
	    String bucketName = appUserForm.getUserName() + "bucket";
	    // making the bucket name unique using time stamps...
	    bucketName += userId;
	    // creating bucket in AWS S3...
	    bucketCreated = service.createBucket(bucketName);
	    ApplicationUserBucket userBucket = new ApplicationUserBucket();
	    userBucket.setId(userId);
	    userBucket.setName(bucketName);
	    userBucket.setUser(user);
	    user.setBucket(userBucket);
	    // saving final user with buckets assigned...
	    userSaved = userDAO.saveUsers(user);
	    // Registration Routines ends here...
	} catch (Exception e) { // Other error!!
	    model.addAttribute("errorMessage", "Error: " + e.getMessage());
	    return "register";
	}

	model.addAttribute("registered", "You have been successfully registered! please login.");

	return (userSaved == true && bucketCreated == true) ? "login" : "register";
    }
}
