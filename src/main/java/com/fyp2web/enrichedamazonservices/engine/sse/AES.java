package com.fyp2web.enrichedamazonservices.engine.sse;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AES {

	private static final Logger LOGGER = LoggerFactory.getLogger(AES.class);
	private SecretKey key;
	private Cipher cipher;

	public AES() throws Exception {
		// this(0, "");
		final byte[] keyBytes = new byte[32];
		final SecureRandom secureRandom = new SecureRandom();
		secureRandom.nextBytes(keyBytes);
		this.key = new SecretKeySpec(keyBytes, "AES");
		System.out.println("my key " + " length" + this.key.getEncoded().length);
		this.cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	}

	public AES(final String encodedKey) throws Exception {
		final byte[] keyBytes = Base64.decodeBase64(encodedKey.getBytes("UTF-8"));
		this.key = new SecretKeySpec(keyBytes, "AES");
		this.cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	}

	public byte[] encrypt(final byte[] input) throws Exception {
		this.cipher.init(1, this.key);
		return this.cipher.doFinal(input);
	}

	public byte[] decrypt(final byte[] input) throws Exception {
		LOGGER.info("CIPHER ENTERED 1.");
		this.cipher.init(2, this.key);
		return this.cipher.doFinal(input); 
	}

	public byte[] getEncoded() {
		return this.key.getEncoded();
	}

	public AES(int test, String input) throws Exception {
		KeyGenerator keygen = KeyGenerator.getInstance("AES"); // key generator to be used with AES algorithm.
		keygen.init(128); // Key size is specified here.
		this.key = keygen.generateKey();
		byte[] key = this.key.getEncoded();
		System.out.println("my key " + java.util.Base64.getEncoder().encodeToString(key) + "  length: " + key.length);
		SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
		// Security.addProvider((Provider) new BouncyCastleProvider());
		this.cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	}

	public static void main(String[] args) {
		try {
			System.out.println(new AES(0, null));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
