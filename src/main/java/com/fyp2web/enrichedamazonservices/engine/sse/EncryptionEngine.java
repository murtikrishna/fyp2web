package com.fyp2web.enrichedamazonservices.engine.sse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncryptionEngine {

	private static final Logger LOGGER = LoggerFactory.getLogger(AES.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static byte[] tryEncryptionExecution(byte[] data, String key) throws Exception {
		// initializing AES key
		AES aes = new AES(key);
		return aes.encrypt(data);
	}

	public static byte[] tryDecryptionExecution(byte[] data, String key) throws Exception {
		// initializing AES key
		LOGGER.info("CIPHER key initiated 1.");
		AES aes = new AES(key);
		LOGGER.info("CIPHER key done.");
		String s=new String(data);
		return aes.decrypt(data);
	}
	
	public static byte[] tryDecryptionExecutionOnBlockCipher(byte[] data, String key) throws Exception {
		// initializing AES key
				LOGGER.info("CIPHER key initiated 1.");
				AES aes = new AES(key);
				LOGGER.info("CIPHER key done.");
				return aes.decrypt(data);
	}
	
	public static byte[] tryEncryptionExecutionOnBlockCipher(byte[] data, String key) throws Exception {
		// initializing AES key
		AES aes = new AES(key);
		return aes.encrypt(data);
	}


}
