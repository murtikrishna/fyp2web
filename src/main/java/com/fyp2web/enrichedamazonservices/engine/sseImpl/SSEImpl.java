package com.fyp2web.enrichedamazonservices.engine.sseImpl;

import javax.crypto.spec.SecretKeySpec;

import java.security.InvalidParameterException;
import java.security.Security;
import java.util.Arrays;

public class SSEImpl implements SymmetricSearchableCipher {

	private AESCipher aesCipher, aesTmpCipher = null;

	private String type;
	public static int BLOCK_SIZE;
	public static int BLOCK_BYTES;
	private byte[] seedBytes;

	private final int left;
	private final int right;

	public SSEImpl(SecretKeySpec spec, String type, double loadFactor, int blockSize) throws InvalidParameterException {

		if (loadFactor <= 0 || loadFactor > 1)
			throw new InvalidParameterException("Invalid Load Factor");

		StreamCipher.init(spec);
		seedBytes = SSEUtil.getRandomBytes(16); // For Nonce
		switch (type) {
		case "AES":
			if (blockSize != 128)
				throw new InvalidParameterException("Invalid Block Size for AES");
			aesCipher = new AESCipher("AES/ECB/PKCS7Padding", spec);
			break;
		default:
			throw new InvalidParameterException("Invalid Block Cipher type");
		}

		// init parameters
		this.type = type;
		BLOCK_SIZE = blockSize;
		BLOCK_BYTES = BLOCK_SIZE / Byte.SIZE;
		this.left = ((int) (loadFactor * BLOCK_BYTES));
		this.right = BLOCK_BYTES - left;
	}

	public byte[] encrypt(byte[] plainBytes, long recordID) throws Exception {

		if (plainBytes == null)
			return new byte[0];

		/* Generate Stream Cipher Bytes */
		byte[] streamCipherBytes = StreamCipher.getRandomStreamOfBytes(recordID, seedBytes);

		byte[] blockCipherBytes = encrypt(plainBytes);

		/* Split the cipher Bytes into {left, Right} */
		byte[] blockCipherBytesLeft = Arrays.copyOfRange(blockCipherBytes, 0, left);
		byte[] streamCipherBytesLeft = Arrays.copyOfRange(streamCipherBytes, 0, left);

		/* Generate search layer */
		byte[] tmpBytes = getSearchLayer(blockCipherBytesLeft, streamCipherBytesLeft);

		byte[] searchLayerBytesRight;
		if (right == 0) // No false positives but additional storage
			searchLayerBytesRight = Arrays.copyOfRange(tmpBytes, 0, BLOCK_BYTES);
		else // Expect false positives while searching
			searchLayerBytesRight = Arrays.copyOfRange(tmpBytes, 0, right);

		byte[] searchLayerBytes = new byte[streamCipherBytesLeft.length + searchLayerBytesRight.length];

		System.arraycopy(streamCipherBytesLeft, 0, searchLayerBytes, 0, left);
		System.arraycopy(searchLayerBytesRight, 0, searchLayerBytes, left, searchLayerBytesRight.length);

		return SSEUtil.xorTwoByteArrays(blockCipherBytes, searchLayerBytes);
	}

	public byte[] decrypt(byte[] cipherBytes, long recordID) throws Exception {
		if (cipherBytes == null)
			return new byte[0];

		/* Generate a Stream Cipher */
		byte[] streamCipherBytes = StreamCipher.getRandomStreamOfBytes(recordID, seedBytes);

		byte[] streamCipherBytesLeft = Arrays.copyOfRange(streamCipherBytes, 0, left);

		/* Split the cipher Bytes into {left, Right} */
		byte[] cipherBytesLeft = Arrays.copyOfRange(cipherBytes, 0, left);
		byte[] cipherBytesRight = Arrays.copyOfRange(cipherBytes, left, cipherBytes.length);

		/*
		 * Peel off the left bytes of search layer from cipherText to get left bytes of
		 * Block Cipher
		 */

		byte[] blockCipherBytesLeft = SSEUtil.xorTwoByteArrays(cipherBytesLeft, streamCipherBytesLeft);

		if (blockCipherBytesLeft.length == BLOCK_BYTES)
			return decrypt(blockCipherBytesLeft);

		else {

			/*
			 * compute the right bytes of search layer
			 */

			byte[] tmpBytes = getSearchLayer(blockCipherBytesLeft, streamCipherBytesLeft);
			byte[] tmpBytesRight = Arrays.copyOfRange(tmpBytes, 0, right);

			byte[] blockCipherBytesRight = SSEUtil.xorTwoByteArrays(cipherBytesRight, tmpBytesRight);

			byte[] blockLayerBytes = new byte[BLOCK_BYTES];
			System.arraycopy(blockCipherBytesLeft, 0, blockLayerBytes, 0, left);
			System.arraycopy(blockCipherBytesRight, 0, blockLayerBytes, left, right);

			return decrypt(blockLayerBytes);
		}
	}

	public byte[] getTrapDoor(byte[] plainBytes) throws Exception {
		return encrypt(plainBytes);
	}

	/*
	 * Blind folded Match !
	 */

	public boolean isMatch(byte[] trapDoorBytes, byte[] cipherBytes) throws Exception {

		if (cipherBytes == null || trapDoorBytes == null)
			return false;

		/* Peel off the search layer bytes of given layer */
		byte[] searchBytes = SSEUtil.xorTwoByteArrays(trapDoorBytes, cipherBytes);
		byte[] searchBytesLeft = Arrays.copyOfRange(searchBytes, 0, left);
		byte[] searchBytesRight = Arrays.copyOfRange(searchBytes, left, cipherBytes.length);

		/* Split the trapDoorBytes into {left, Right} of Left bytes and Right bytes */
		byte[] trapDoorBytesLeft = Arrays.copyOfRange(trapDoorBytes, 0, left);

		/* Verify search layer */
		byte[] tmpBytes = getSearchLayer(trapDoorBytesLeft, searchBytesLeft);
		byte[] tmpBytesRight;
		if (right == 0)
			tmpBytesRight = Arrays.copyOfRange(tmpBytes, 0, BLOCK_BYTES);
		else
			tmpBytesRight = Arrays.copyOfRange(tmpBytes, 0, right);

		return Arrays.equals(searchBytesRight, tmpBytesRight);

	}

	public byte[] encrypt(byte[] plainBytes) throws Exception {
		switch (type) {
		case "AES":
			return aesCipher.encrypt(plainBytes);
		default:
			throw new InvalidParameterException("Invalid Block Cipher type");
		}
	}

	public byte[] decrypt(byte[] cipherBytes) throws Exception {
		switch (type) {
		case "AES":
			return aesCipher.decrypt(cipherBytes);
		default:
			throw new InvalidParameterException("Invalid Block Cipher type");
		}
	}

	private byte[] getSearchLayer(byte[] key, byte[] data) throws Exception {
		switch (type) {
		case "AES":
			if (aesTmpCipher == null)
				aesTmpCipher = new AESCipher("AES/ECB/PKCS7Padding", key);
			return aesTmpCipher.encrypt(data);
		default:
			throw new InvalidParameterException("Invalid Block Cipher type");
		}
	}

	// testing the SSE engine...
	public static void main(String[] args) throws Exception {

		// setting system security properties...
		Security.addProvider((java.security.Provider) new org.bouncycastle.jce.provider.BouncyCastleProvider());
		SecretKeySpec keySpec = new SecretKeySpec("0UMiiBnCvl2oUstAK0sQvw==".getBytes(), "AES");
		SSEImpl sse = new SSEImpl(keySpec, "AES", 1, 128);
		byte[] b = sse.encrypt("hello ji chacha ji kase ho aap".getBytes(), 0);
		byte[] c = sse.decrypt(b, 0);
		System.out.println(new String(c) + " is searchable:-\n" + sse.isMatch(sse.getTrapDoor("hello ji".getBytes()), b));
	}

}
