package com.fyp2web.enrichedamazonservices.engine.sseImpl;
public interface SymmetricSearchableCipher {

    byte[] encrypt(byte[] plainBytes) throws Exception;

    byte[] decrypt(byte[] cipherBytes)throws Exception;

    byte[] getTrapDoor(byte[] plainBytes) throws Exception;

    public boolean isMatch(byte[] trapDoorBytes, byte[] cipherBytes)throws Exception;
}
