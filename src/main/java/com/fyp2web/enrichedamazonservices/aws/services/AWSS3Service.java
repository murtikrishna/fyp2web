package com.fyp2web.enrichedamazonservices.aws.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface AWSS3Service {

    public void uploadFile(final MultipartFile multipartFile) throws Exception;

    public byte[] downloadFile(final String keyName) throws Exception;

    public boolean searchFile(final String keyName) throws Exception;

    public List<String> searchFileOverContent(final String keyName) throws Exception;

    public boolean deleteFile(String fileName) throws Exception;

    public boolean createBucket(String bucket) throws Exception;
}
