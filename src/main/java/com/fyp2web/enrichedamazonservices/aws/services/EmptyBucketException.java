package com.fyp2web.enrichedamazonservices.aws.services;

public class EmptyBucketException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public EmptyBucketException(String message) {
	super(message);
    }
}
