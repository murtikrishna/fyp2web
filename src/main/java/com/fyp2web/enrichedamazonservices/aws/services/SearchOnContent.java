package com.fyp2web.enrichedamazonservices.aws.services;

import java.security.Security;

import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.fyp2web.enrichedamazonservices.engine.sseImpl.SSEImpl;

public class SearchOnContent {

    public static void main(String[] args) {
	// TODO Auto-generated method stub

    }

    public boolean carrySearch(String keyName, byte[] content) {

	SecretKeySpec spec = null;
	Security.addProvider(new BouncyCastleProvider());
	boolean found = false;
	try {
	    SSEImpl sse = new SSEImpl(spec, "AES", 1, 128);
	    found = sse.isMatch(keyName.getBytes(), content);
	} catch (Exception e) {
	    e.printStackTrace();
	}

	// making it lowercase....to search case-insensitive on contents...
	String data = new String(content);
	data = data.toLowerCase();
	keyName = keyName.toLowerCase();
	found = data.contains(keyName);
	return found;
    }

}
