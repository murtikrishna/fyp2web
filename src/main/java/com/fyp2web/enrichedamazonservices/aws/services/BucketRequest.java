package com.fyp2web.enrichedamazonservices.aws.services;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.GetBucketLocationRequest;

public class BucketRequest {

    public static String create(String bucket, AmazonS3 s3Client) throws Exception {
	try {
	    if (!s3Client.doesBucketExistV2(bucket)) {
		// Because the CreateBucketRequest object doesn't specify a region, the
		// bucket is created in the region specified in the client.
		s3Client.createBucket(new CreateBucketRequest(bucket));
		// Verify that the bucket was created by retrieving it and checking its
		// location.
		String bucketLocation = s3Client.getBucketLocation(new GetBucketLocationRequest(bucket));
		System.out.println("Bucket location: " + bucketLocation);
		return bucketLocation;
	    }
	} catch (AmazonServiceException e) {
	    throw e;
	} catch (SdkClientException e) {
	    throw e;
	}
	return null;
    }
}
