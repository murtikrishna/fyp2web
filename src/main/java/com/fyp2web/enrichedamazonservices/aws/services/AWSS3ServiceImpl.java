package com.fyp2web.enrichedamazonservices.aws.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;
import com.fyp2web.enrichedamazonservices.engine.sse.EncryptionEngine;
import com.fyp2web.enrichedamazonservices.user.services.ApplicationUser;

@Service
public class AWSS3ServiceImpl implements AWSS3Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(AWSS3ServiceImpl.class);

    @Autowired
    private AmazonS3 amazonS3;
    @Autowired
    private ApplicationContext context;
    private ApplicationUser user = null;

    @Override
    // @Async annotation ensures that the method is executed in a different
    // background thread
    // but not consume the main thread.
    @Async
    public void uploadFile(final MultipartFile multipartFile) throws Exception {
	LOGGER.info("File upload in progress.");
	try {
	    final File file = convertMultiPartFileToFile(multipartFile);
	    LOGGER.info("Encryption Done Successfully.");
	    user = (ApplicationUser) context.getBean(ApplicationUser.class);
	    uploadFileToS3Bucket(user.getBucket().getName(), file);
	    LOGGER.info("File upload is completed.");
	    file.delete(); // To remove the file locally created in the project folder.
	} catch (final AmazonServiceException ex) {
	    LOGGER.info("File upload is failed.");
	    LOGGER.error("Error= {} while uploading file.", ex.getMessage());
	    throw ex;
	}
    }

    private File convertMultiPartFileToFile(final MultipartFile multipartFile) throws Exception {
	final File file = new File(multipartFile.getOriginalFilename());
	try (final FileOutputStream outputStream = new FileOutputStream(file)) {
	    user = (ApplicationUser) context.getBean(ApplicationUser.class);
	    byte[] encryptedData = EncryptionEngine.tryEncryptionExecution(multipartFile.getBytes(),
		    user.getAeskey().getKey());
	    outputStream.write(encryptedData);
	} catch (final IOException ex) {
	    LOGGER.error("Error converting the multi-part file to file= ", ex.getMessage());
	    throw new RuntimeException(
		    String.format("Error converting the multi-part file to file= ", ex.getMessage()));
	} catch (BeansException e) {
	    LOGGER.error("Error getting beans= ", e.getMessage());
	} catch (Exception e) {
	    LOGGER.error("Error while encrypting= ", e.getMessage());
	}
	return file;
    }

    private void uploadFileToS3Bucket(final String bucketName, final File file) throws Exception {
	try {
	    final String uniqueFileName = file.getName();
	    user = (ApplicationUser) context.getBean(ApplicationUser.class);
	    byte[] encryptedFileName = EncryptionEngine.tryEncryptionExecution(uniqueFileName.getBytes(),
		    user.getAeskey().getKey());
	    LOGGER.info("Uploading file with name= " + uniqueFileName);
	    String s = new String(encryptedFileName);
	    final PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
		    Base64.encodeBase64String(encryptedFileName), file);
	    amazonS3.putObject(putObjectRequest);
	} catch (Exception e) {
	    throw new RuntimeException("Error while executing amazon service: " + e.getMessage());
	}
    }

    @Override
    // @Async annotation ensures that the method is executed in a different
    // background thread
    // but not consume the main thread.
    @Async
    public byte[] downloadFile(final String keyName) throws Exception {
	byte[] content = null;
	LOGGER.info("Downloading an object with key= " + keyName);
	user = (ApplicationUser) context.getBean(ApplicationUser.class);
	byte[] encryptedKeyName = EncryptionEngine.tryEncryptionExecution(keyName.getBytes(),
		user.getAeskey().getKey());
	final S3Object s3Object = amazonS3.getObject(user.getBucket().getName(),
		Base64.encodeBase64String(encryptedKeyName));
	final S3ObjectInputStream stream = s3Object.getObjectContent();
	try {
	    content = IOUtils.toByteArray(stream);
	    LOGGER.info("File downloaded successfully." + (content != null));
	    content = EncryptionEngine.tryDecryptionExecution(content, user.getAeskey().getKey());
	    LOGGER.info("File decrypted successfully.");
	    s3Object.close();
	} catch (final IOException ex) {
	    LOGGER.info("IO Error Message= " + ex.getMessage());
	    throw new RuntimeException("IO Error Message= " + ex.getMessage());
	}
	return content;
    }

    @Override
    // @Async annotation ensures that the method is executed in a different
    // background thread
    // but not consume the main thread.
    @Async
    public boolean searchFile(final String keyName) throws Exception {
	boolean found = false;
	LOGGER.info("Searching an object with key= " + keyName);
	user = (ApplicationUser) context.getBean(ApplicationUser.class);
	byte[] encryptedKeyName = EncryptionEngine.tryEncryptionExecution(keyName.getBytes(),
		user.getAeskey().getKey());
	try {
	    ObjectListing listing;
	    do {
		listing = amazonS3.listObjects(user.getBucket().getName());
		found = listing.getObjectSummaries().stream()
			.anyMatch(e -> e.getKey().equals(Base64.encodeBase64String(encryptedKeyName)));
	    } while (found == false && listing.isTruncated());
	    if (found)
		LOGGER.info("Search found.");
	} catch (final Exception ex) {
	    LOGGER.info("AWS service Error Message= " + ex.getMessage());
	    throw new RuntimeException("AWS service Error Message= " + ex.getMessage());
	}
	return found;
    }

    @Override
    // @Async annotation ensures that the method is executed in a different
    // background thread
    // but not consume the main thread.
    @Async
    public List<String> searchFileOverContent(final String keyName) throws Exception {
	boolean found = false;
	List<String> list = new ArrayList<String>();
	user = (ApplicationUser) context.getBean(ApplicationUser.class); // loading user context...
	// check if fileName is set to any
	if (keyName.length() > 0) {
	    LOGGER.info("Searching an object with key= " + keyName);
	    // searching the keys....
	    if (searchFile(keyName)) {
		list.add(keyName);
	    }
	}

	try {
	    /// searching in the content....
	    SearchOnContent search = new SearchOnContent();
	    S3Object s3Object = null;
	    S3ObjectInputStream stream = null;
	    byte[] content = null;
	    ObjectListing listing;
	    byte[] decryptedKeyName = null;
	    String logs = "";
	    int counter = 0; // loop counter
	    do {
		listing = amazonS3.listObjects(user.getBucket().getName());
		if (keyName.length() > 0) {
		    inner: for (S3ObjectSummary e : listing.getObjectSummaries()) {
			s3Object = amazonS3.getObject(user.getBucket().getName(), e.getKey());
			stream = s3Object.getObjectContent();
			content = IOUtils.toByteArray(stream);
			try {
			    try {
				content = EncryptionEngine.tryDecryptionExecution(content, user.getAeskey().getKey());
			    } catch (Exception e1) {
				continue inner;
			    }
			    if (search.carrySearch(keyName, content)) {
				decryptedKeyName = EncryptionEngine.tryDecryptionExecution(
					Base64.decodeBase64(e.getKey().getBytes()), user.getAeskey().getKey());
				list.add(new String(decryptedKeyName));
			    }
			} catch (Exception e2) {
			    counter++;
			    LOGGER.info("Decryption of file name Error Message= " + e2.getMessage());
			    logs += e2.getMessage() + ";\n";
			}
		    }
		} else { // indicates search all files in the bucket...
		    for (S3ObjectSummary e : listing.getObjectSummaries()) {
			counter++;
			try {
			    System.out.println("hello");
			    list.add(new String(EncryptionEngine.tryDecryptionExecution(
				    Base64.decodeBase64(e.getKey().getBytes()), user.getAeskey().getKey())));
			} catch (Exception e1) {
			    LOGGER.info("Decryption of file name Error Message= " + e1.getMessage());
			    logs += e1.getMessage() + ";\n";
			}

		    }
		}
	    } while (listing.isTruncated());
	    if (list.size() == 0 && counter > 0)
		throw new RuntimeException(
			"AWS service Error Message: \n while decrypting file name, file name be a plain text and not encrypted: "
				+ logs);
	    if (keyName.length() == 0 && counter == 0) {
		throw new EmptyBucketException("file not found: No files in the bucket");
	    }

	} catch (final Exception ex) {
	    LOGGER.info("AWS service Error Message= \n" + ex.getMessage());
	    throw ex;
	}
	return list;
    }

    @Override
    // @Async annotation ensures that the method is executed in a different
    // background thread
    // but not consume the main thread.
    @Async
    public boolean deleteFile(String fileName) throws Exception {
	user = (ApplicationUser) context.getBean(ApplicationUser.class);
	byte[] encryptedKeyName = EncryptionEngine.tryEncryptionExecution(fileName.getBytes(),
		user.getAeskey().getKey());
	boolean found = false;
	boolean deleted = false;
	// deleting object in amazon s3...
	amazonS3.deleteObject(user.getBucket().getName(), Base64.encodeBase64String(encryptedKeyName));
	// checking if the file is deleted or not...
	try {
	    ObjectListing listing;
	    do {
		listing = amazonS3.listObjects(user.getBucket().getName());
		found = listing.getObjectSummaries().stream()
			.anyMatch(e -> e.getKey().equals(Base64.encodeBase64String(encryptedKeyName)));
	    } while (found == false && listing.isTruncated());
	    if (!found) {
		LOGGER.info("Not found...this make sure File is deleted ");
		deleted = !found;
	    }
	} catch (final Exception ex) {
	    LOGGER.info("AWS service Error Message= " + ex.getMessage());
	    throw new RuntimeException("AWS service Error Message= " + ex.getMessage());
	}
	return deleted;
    }

    @Override
    public boolean createBucket(String bucket) throws Exception {
	String location = BucketRequest.create(bucket, amazonS3);
	return location.equalsIgnoreCase(amazonS3.getRegionName()) ? true : false;
    }

    public static void main(String[] args) throws Exception {
	byte[] b = null;
	b = EncryptionEngine.tryEncryptionExecution("hello".getBytes(), "HzjfW7lHXi6K+dUHG04Bg5TIlnNBFtktCX37S8HlGb4=");
	String s = new String(b);

	System.out.println(Base64.encodeBase64String(b));
	byte[] c = EncryptionEngine.tryDecryptionExecution(b, "HzjfW7lHXi6K+dUHG04Bg5TIlnNBFtktCX37S8HlGb4=");
	System.out.println(new String(c));
    }
}
