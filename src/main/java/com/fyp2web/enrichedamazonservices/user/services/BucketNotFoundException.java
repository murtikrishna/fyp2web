package com.fyp2web.enrichedamazonservices.user.services;

public class BucketNotFoundException extends RuntimeException {

    public BucketNotFoundException() {
	super("AWS Bucket not found with the user name specified : ");
    }
}
