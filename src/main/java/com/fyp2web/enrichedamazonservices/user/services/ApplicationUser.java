package com.fyp2web.enrichedamazonservices.user.services;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "APP_USERS")
public class ApplicationUser {

    @Id
    @Column(name = "ID")
    private long id;
    @Column(name = "USERNAME")
    private String username;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "FULLNAME")
    private String fullName;
    @Column(name = "GENDER")
    private char gender;

    /**
     * @return the fullName
     */
    public String getFullName() {
	return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String... names) {
	if (names.length == 2)
	    this.fullName = names[0] + " " + names[1];
	else
	    this.fullName = names[0];
    }

    /**
     * @return the gender
     */
    public char getGender() {
	return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
	this.gender = gender.charAt(0);
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    @ManyToOne
    @JoinColumn(name = "AES_KEY")
    private AESKey aeskey;

    @OneToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "BUCKET")
    private ApplicationUserBucket bucket;

    /**
     * @return the bucket
     */
    public ApplicationUserBucket getBucket() {
	return bucket;
    }

    /**
     * @param bucket the bucket to set
     */
    public void setBucket(ApplicationUserBucket bucket) {
	this.bucket = bucket;
    }

    /**
     * @return the aesKey from DB to support encryption
     */
    public AESKey getAeskey() {
	return aeskey;
    }

    /**
     * @param aeskey the aeskey to set
     */
    public void setAeskey(AESKey aeskey) {
	this.aeskey = aeskey;
    }

    public long getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
	this.id = id;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

}
