package com.fyp2web.enrichedamazonservices.user.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserAccessDAOImpl implements UserAccessDAO {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;
    @Autowired
    private AESKeyRepository aesKeyRepository;
    @Autowired
    private ApplicationUserBucketRepository applicationUserBucketRepository;

    public UserAccessDAOImpl() {

    }

    @Override
    public Long findByName(String userName) {
	Optional<ApplicationUser> applicationUser = applicationUserRepository.findByUsername(userName);
	return applicationUser.isPresent() ? applicationUser.get().getId() : null;
    }

    public AESKey getAES() {
	Optional<AESKey> aesKeyInstance = aesKeyRepository.findById(1);
	aesKeyInstance.orElseThrow(() -> new UsernameNotFoundException("Key not found"));
	return aesKeyInstance.isPresent() ? aesKeyInstance.get() : null;
    }

    @Override
    public Long findByEmail(String email) {
	Optional<ApplicationUser> applicationUser = applicationUserRepository.findByEmail(email);
	return applicationUser.isPresent() ? applicationUser.get().getId() : null;
    }

    @Override
    public boolean saveUsers(ApplicationUser user) {
	// user build up before saving to repository context..
	if (user.getAeskey() == null) {
	    try {
		if (getAES() != null)
		    user.setAeskey(getAES());
	    } catch (Exception e) {
		throw e;
	    }
	}
	// saving objects to repository...
	applicationUserRepository.save(user);
	return true;
    }

}
