/**
 * 
 */
package com.fyp2web.enrichedamazonservices.user.services;

/**
 * @author hw462307
 *
 */
public interface UserAccessDAO {

	public Long findByName(String userName);
	public boolean saveUsers(ApplicationUser user);
	public Long findByEmail(String email);
}
