package com.fyp2web.enrichedamazonservices.user.services;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class ApplicationUserDetails implements UserDetails {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String username;
    private String password;
    private boolean active;

    /**
     * 
     */
    public ApplicationUserDetails(ApplicationUser user) {
	this.username = user.getUsername();
	this.password = user.getPassword();
	this.active = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getPassword() {
	return this.password;
    }

    @Override
    public String getUsername() {
	// TODO Auto-generated method stub
	return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
	// TODO Auto-generated method stub
	return true;
    }

    @Override
    public boolean isAccountNonLocked() {
	// TODO Auto-generated method stub
	return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
	// TODO Auto-generated method stub
	return true;
    }

    @Override
    public boolean isEnabled() {
	// TODO Auto-generated method stub
	return true;
    }

}
