package com.fyp2web.enrichedamazonservices.user.services;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationUserBucketRepository extends JpaRepository<ApplicationUserBucket, Long> {

}
