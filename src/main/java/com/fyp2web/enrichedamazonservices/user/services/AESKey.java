package com.fyp2web.enrichedamazonservices.user.services;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AES_RECORD")
public class AESKey {

    /**
     * @return the user
     */
    public Set<ApplicationUser> getUser() {
	return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(Set<ApplicationUser> user) {
	this.user = user;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private int id;

    /**
     * @return the id
     */
    public int getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
	this.id = id;
    }

    /**
     * @return the key
     */
    public String getKey() {
	return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
	this.key = key;
    }

    @Column(name = "AES_KEY")
    private String key;

    @OneToMany(mappedBy = "aeskey")
    Set<ApplicationUser> user;

    public static void main(String[] args) {
	// TODO Auto-generated method stub

    }

}
