package com.fyp2web.enrichedamazonservices.user.services;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BUCKET")
public class ApplicationUserBucket {

    @Id
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME")
    private String name;

    @OneToOne(mappedBy = "bucket")
    private ApplicationUser user;

    /**
     * @return the user
     */
    public ApplicationUser getUser() {
	return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(ApplicationUser user) {
	this.user = user;
    }

    /**
     * @return the name of the bucket from repository
     */
    public String getName() {
	return name;
    }

    /**
     * @return the id
     */
    public long getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
	this.id = id;
    }

    /**
     * @param name the name to set the name of bucket from repository
     */
    public void setName(String name) {
	this.name = name;
    }

}
