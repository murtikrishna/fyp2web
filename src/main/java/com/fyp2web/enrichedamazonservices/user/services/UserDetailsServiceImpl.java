package com.fyp2web.enrichedamazonservices.user.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.GenericWebApplicationContext;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;
    @Autowired
    private GenericWebApplicationContext context;
    private int count = 0;

    public UserDetailsServiceImpl() {

    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
	count++;
	Optional<ApplicationUser> applicationUser = applicationUserRepository.findByEmail(userName);
	System.out.println(applicationUser.get().getAeskey().getKey());
	// creating bean for future access...
	try {
	    for (String name : context.getBeanDefinitionNames()) {
		System.out.println(name);
	    }
	    if (context.containsBean("com.fyp2web.enrichedamazonservices.user.services.ApplicationUser"))
		context.removeBeanDefinition("com.fyp2web.enrichedamazonservices.user.services.ApplicationUser");
	    context.registerBean(ApplicationUser.class, () -> new UserBeans().getUser(applicationUser.get()));
	} catch (Exception e) {
	    System.out.println(e);
	}
	applicationUser.orElseThrow(() -> new UsernameNotFoundException(userName));
	return applicationUser.map(ApplicationUserDetails::new).get();
    }

}